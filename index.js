/*
1. In the s24 folder, create an activity folder and an index.html and index.js file inside of it.
2. Link the index.js file to the index.html file.
[done] 3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
[done] 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
[done] 5. Create a variable address with a value of an array containing details of an address.
[done] 6. Destructure the array and print out a message with the full address using Template Literals.
[done] 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
[done] 8. Destructure the object and print out a message with the details of the animal using Template Literals.
[done] 9. Create an array of numbers.
[done] 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
[done] 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
[done] 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
[done] 13. Create/instantiate a new object from the class Dog and console log the object.
14. Create a git repository named s24.
15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
16. Add the link in Boodle.
*/

//Activity 1
let num = 2;
let getCube = num ** 3;
console.log(`The cube of ${num} is ${getCube}.`);

//Activity 2
let address = [258, "Washington Ave NW", "California", 90011];

let [buildingNum, street, state, postalCode] = address;
console.log(`I live at ${buildingNum} ${street}, ${state} ${postalCode}.`);

//Activity 3
let animal = {
	name: "Lolong",
	type: "salt water crocodile",
	weight: 1075,
	length: "20 ft 3 in"
}

let {name, type, weight, length} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${length}.`);

//Activity 4
let numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => console.log(number));

//Activity 5
let sumOfArray = numbers.reduce((x, y) => {
	return	x + y;
});

console.log(sumOfArray);

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;	
	}

}

let dog = new Dog("Frankie", 5, "Miniature Dachshund");

console.log(dog);

